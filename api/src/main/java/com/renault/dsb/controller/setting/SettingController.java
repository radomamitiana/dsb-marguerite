package com.renault.dsb.controller.setting;

import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.data.dto.SettingsDTO;
import com.renault.dsb.service.setting.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/setting")
public class SettingController {

    @Autowired
    private SettingService settingService;

    /**
     * @param settingDTO
     * @return
     */
    @PostMapping(value = {"/createOrUpdate"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public SettingDTO createOrUpdate(@Valid @RequestBody SettingDTO settingDTO) {

        return settingService.createOrUpdate(settingDTO);
    }

    /**
     * @param id
     * @return
     */
    @DeleteMapping(path = "/delete", params = {"id"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public IsDeletedDTO delete(@RequestParam(value = "id") Integer id) {
        return settingService.delete(id);
    }

    /**
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = {"/list"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public SettingsDTO listSettings(@RequestParam(value = "page", defaultValue = "0", required = false) int page,
                                    @RequestParam(value = "size", defaultValue = "10", required = false) int size) {
        return settingService.listSettings(PageRequest.of(page, size));
    }

    /**
     * Search something in the setting list
     *
     * @param stringQuery
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = {"/list"}, params = {"stringQuery"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public SettingsDTO searchSettings(@RequestParam(value = "stringQuery", required = false) String stringQuery,
                                      @RequestParam(value = "page", defaultValue = "0", required = false) int page,
                                      @RequestParam(value = "size", defaultValue = "10", required = false) int size) {
        return settingService.listSettings(stringQuery, PageRequest.of(page, size));
    }

}
