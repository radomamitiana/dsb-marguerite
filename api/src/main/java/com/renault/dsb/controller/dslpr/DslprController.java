package com.renault.dsb.controller.dslpr;

import com.renault.dsb.data.constants.DslprType;
import com.renault.dsb.data.dto.ResponseDTO;
import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.service.dslpr.DslprService;
import com.renault.dsb.service.setting.SettingService;
import com.renault.dsb.service.utils.CryptoUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/api/dslpr")
public class DslprController {

    @Autowired
    private DslprService dslprService;

    @Autowired
    private SettingService settingService;

    /**
     * download dslpr File
     *
     * @return Resource
     */
    @GetMapping(path = "/download", params = {"type"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Resource> download(@RequestParam(value = "type") String type, @RequestParam(value = "ipn") String currentIpn) {

        SettingDTO settingIpn = null;
        SettingDTO settingPassword = null;
        SettingDTO settingPath = null;

        switch (type) {
            case DslprType
                    .CAUSE_ALE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.caledev.path");
                break;
            case DslprType
                    .CORRESP_ISO_CODEPAYS:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.codepays.path");
                break;
            case DslprType
                    .CORRESPONDANCES:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.tb_corresp.path");
                break;
            case DslprType
                    .FACTOR_CAISSE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.load_factor_caisse.path");
                break;
            case DslprType
                    .FACTOR_COMMERCE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.load_factor_commerce.path");
                break;
            case DslprType
                    .PRIX_MOYEN:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.table_prix_moyen.path");
                break;
            default:
                break;
        }

        if (settingIpn != null && !StringUtils.isEmpty(settingIpn.getValue()) && settingPassword != null
                && !StringUtils.isEmpty(settingPassword.getValue()) && settingPath != null
                && !StringUtils.isEmpty(settingPath.getValue())) {
            CryptoUtil cryptoUtil = new CryptoUtil();
            Resource file = dslprService.download(settingIpn.getValue(),
                    cryptoUtil.decrypt(settingPassword.getValue()), settingPath.getValue(), type, currentIpn);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        }
        return null;
    }

    /**
     * upload dslpr file
     *
     * @return ResponseDTO
     */
    @PostMapping(path = "/upload", params = {"type"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseDTO upload(@RequestParam(value = "type") String type, @RequestParam("file") MultipartFile multipartFile, @RequestParam(value = "ipn") String currentIpn) {

        SettingDTO settingIpn = null;
        SettingDTO settingPassword = null;
        SettingDTO settingPath = null;
        if (DslprType
                .CAUSE_ALE.equals(type)) {
            settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
            settingPassword = settingService.getValueByName("hadoop.dsb.password");
            settingPath = settingService.getValueByName("hadoop.dsb.dslpr.caledev.path");

        } else if (DslprType
                .CORRESP_ISO_CODEPAYS.equals(type)) {
            settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
            settingPassword = settingService.getValueByName("hadoop.dsb.password");
            settingPath = settingService.getValueByName("hadoop.dsb.dslpr.codepays.path");

        } else if (DslprType
                .CORRESPONDANCES.equals(type)) {
            settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
            settingPassword = settingService.getValueByName("hadoop.dsb.password");
            settingPath = settingService.getValueByName("hadoop.dsb.dslpr.tb_corresp.path");

        } else if (DslprType
                .FACTOR_CAISSE.equals(type)) {
            settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
            settingPassword = settingService.getValueByName("hadoop.dsb.password");
            settingPath = settingService.getValueByName("hadoop.dsb.dslpr.load_factor_caisse.path");

        } else if (DslprType
                .FACTOR_COMMERCE.equals(type)) {
            settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
            settingPassword = settingService.getValueByName("hadoop.dsb.password");
            settingPath = settingService.getValueByName("hadoop.dsb.dslpr.load_factor_commerce.path");

        } else if (DslprType
                .PRIX_MOYEN.equals(type)) {
            settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
            settingPassword = settingService.getValueByName("hadoop.dsb.password");
            settingPath = settingService.getValueByName("hadoop.dsb.dslpr.table_prix_moyen.path");

        }

        if (settingIpn != null && !StringUtils.isEmpty(settingIpn.getValue()) && settingPassword != null
                && !StringUtils.isEmpty(settingPassword.getValue()) && settingPath != null
                && !StringUtils.isEmpty(settingPath.getValue())) {
            CryptoUtil cryptoUtil = new CryptoUtil();
            return dslprService.upload(settingIpn.getValue(),
                    cryptoUtil.decrypt(settingPassword.getValue()), settingPath.getValue(), multipartFile, type, currentIpn);
        }
        return null;
    }

    /**
     * Download error report endpoint
     *
     * @return file
     */
    @GetMapping(path = "/downloadErrorReport/{errorReport}")
    public ResponseEntity<Resource> downloadErrorReport(@PathVariable("errorReport") String errorReport) {
        Resource file = dslprService.downloadErrorReport(errorReport);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }
}
