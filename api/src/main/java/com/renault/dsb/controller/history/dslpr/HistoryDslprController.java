package com.renault.dsb.controller.history.dslpr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.renault.dsb.data.dto.HistorysDTO;
import com.renault.dsb.service.history.dslpr.HistoryDslprService;

@RestController
@RequestMapping(value = "/api/history/dslpr")
public class HistoryDslprController {

    @Autowired
    private HistoryDslprService historyService;

    /**
     * get history list
     *
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = {"/list"}, params = {"type"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public HistorysDTO listHistorys(@RequestParam(value = "type") String type,
                                    @RequestParam(value = "page", defaultValue = "0", required = false) int page,
                                    @RequestParam(value = "size", defaultValue = "5", required = false) int size) {
        return historyService.listHistorys(type, PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "date")));
    }

    /**
     * search endpoint (search anything in the story table)
     *
     * @param stringQuery
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = {"/list"}, params = {"stringQuery", "type"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public HistorysDTO searchHistorys(
            @RequestParam(value = "type") String type,
            @RequestParam(value = "stringQuery", required = false) String stringQuery,
            @RequestParam(value = "page", defaultValue = "0", required = false) int page,
            @RequestParam(value = "size", defaultValue = "5", required = false) int size) {
        return historyService.listHistorys(type, stringQuery,
                PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "date")));
    }

}
