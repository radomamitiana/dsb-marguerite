package com.renault.dsb;

import com.renault.dsb.common.ssl.SSLUtilities;
import com.renault.dsb.service.user.RoleService;
import com.renault.dsb.service.user.UserService;
import com.renault.dsb.service.utils.UtilService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MargueriteApplication implements CommandLineRunner {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	public static void main(String[] args) {
		SSLUtilities.trustAllHostnames();
		SSLUtilities.trustAllHttpsCertificates();
		SpringApplication.run(MargueriteApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		roleService.initRoles();
		userService.initUser();
		UtilService.initStorage();
		UtilService.initFileReportError();
	}
}
