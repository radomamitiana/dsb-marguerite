package com.renault.dsb.repository;

import com.renault.dsb.data.entity.HistoryDslpr;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoryDslprRepository extends JpaRepository<HistoryDslpr, Integer> {

    List<HistoryDslpr> findByIpnContainingOrActionContainingOrStateContainingOrCreatedDateContaining(String ipn,
                                                                                                     String action, String state, String date);


    Page<HistoryDslpr> findByType(String type, Pageable pageable);
}
