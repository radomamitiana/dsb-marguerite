package com.renault.dsb.data.constants;

public class DslprType {
    public static final String CAUSE_ALE = "cause_ale";
    public static final String FACTOR_CAISSE = "factor_caisse";
    public static final String FACTOR_COMMERCE = "factor_commerce";
    public static final String CORRESP_ISO_CODEPAYS = "corresp_iso_codepays";
    public static final String CORRESPONDANCES = "correspondances";
    public static final String PRIX_MOYEN = "prix_moyen";
}
