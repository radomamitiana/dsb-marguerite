package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DropdownDataDTO {
    private Integer id;
    private String value;
}
