package com.renault.dsb.data.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryDslprDTO {
	private Integer id;
	private String ipn;
	private String action;
	private String state;
	private LocalDateTime date;
	private int sizeAddLines;
	private String errorReport;
	private boolean hasError;
	private String createdDate;
	private String type;
}
