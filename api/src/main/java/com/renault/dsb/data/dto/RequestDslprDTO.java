package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestDslprDTO {
    private String path;
    private String fileName;
}
