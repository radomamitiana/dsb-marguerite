package com.renault.dsb.data.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "history_dslpr")
public class HistoryDslpr {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String ipn;
	private String action;
	private String state;
	private LocalDateTime date;
	private int sizeAddLines;
	private String errorReport;
	private boolean hasError;
	private String createdDate;
	private String type;

}
