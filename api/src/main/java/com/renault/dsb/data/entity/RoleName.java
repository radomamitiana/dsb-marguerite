package com.renault.dsb.data.entity;

public enum  RoleName {
    ROLE_ADMIN,
    ROLE_CO2,
    ROLE_TOP_ALLIANCE,
    ROLE_DSLPR,
    ROLE_MONITORING
}