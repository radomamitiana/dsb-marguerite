package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SettingDTO {
	private Integer id;
	private String name;
	private String value;
	private Boolean isCrypt;
}
