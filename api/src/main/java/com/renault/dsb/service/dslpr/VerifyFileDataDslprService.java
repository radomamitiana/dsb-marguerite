package com.renault.dsb.service.dslpr;

import org.springframework.util.StringUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.StringTokenizer;

public interface VerifyFileDataDslprService {

    /**
     * Control if the file contains has right number columns
     *
     * @param stringTokenizerFirstLine
     * @param writer
     * @param hasError
     * @param index
     * @return
     * @throws IOException
     */
    default boolean verifyColumn(int column, StringTokenizer stringTokenizerFirstLine, BufferedWriter writer, boolean hasError, int index) throws IOException {
        if (stringTokenizerFirstLine.countTokens() != column) {
            writer.write("Line " + index + " : The number of column is not " + column + ".\n");
            hasError = true;
        }
        return hasError;
    }

    /**
     * Verify the file cause_ale_deviation
     *
     * @param line
     * @param writer
     * @param index
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileCauseAle(String line, BufferedWriter writer, int index, boolean hasError) throws IOException {
        StringTokenizer stringTokenizer = new StringTokenizer(line, ";");
        String vin = stringTokenizer.nextToken();
        if (line.charAt(0) == ';' || StringUtils.isEmpty(vin)) {
            writer.write("Line " + index + " : The column \"VIN\" is absent.\n");
            hasError = true;
        }

        // Control the column semaine
        String semaine = null;
        while (stringTokenizer.hasMoreTokens()) {
            semaine = stringTokenizer.nextToken();
        }
        if (StringUtils.isEmpty(semaine) || !semaine.matches("\\d+")) {
            writer.write("Line " + index + " : The column \"semaine\" is not an integer.\n");
            hasError = true;
        }
        return hasError;
    }

    /**
     * Verify the file code pays
     *
     * @param line
     * @param writer
     * @param index
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileCodePays(String line, BufferedWriter writer, int index, boolean hasError) throws IOException {
        return verifyAllLineHasNotEmpty(line, writer, index, hasError);
    }

    /**
     * Verify the file Table correspondance
     *
     * @param line
     * @param writer
     * @param index
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileTableCorrespondances(String line, BufferedWriter writer, int index, boolean hasError) throws IOException {
        return hasError;
    }

    /**
     * Verify the file load factor caisse
     *
     * @param line
     * @param writer
     * @param index
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileLoadFactorCaisse(String line, BufferedWriter writer, int index, boolean hasError) throws IOException {
        return verifyColumnIsAbsentAndOtherColumnMightTypeDecimalOrNull("mod_caisse", line, writer, index, hasError);
    }

    /**
     * Verify the file load factor commerce
     *
     * @param line
     * @param writer
     * @param index
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileFactorCommerce(String line, BufferedWriter writer, int index, boolean hasError) throws IOException {
        return verifyColumnIsAbsentAndOtherColumnMightTypeDecimalOrNull("mod_commerce", line, writer, index, hasError);
    }

    /**
     * Verify the file Prix moyen
     *
     * @param line
     * @param writer
     * @param index
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFilePrixMoyen(String line, BufferedWriter writer, int index, boolean hasError) throws IOException {
        StringTokenizer stringTokenizer = new StringTokenizer(line, ";");
        String modCaisse = stringTokenizer.nextToken();
        if (line.charAt(0) == ';' || StringUtils.isEmpty(modCaisse)) {
            writer.write("Line " + index + " : The column h020codedptmo is absent.\n");
            hasError = true;
        }

        // Control the column semaine
        boolean hasWrongValue = false;
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            String content = stringTokenizer.nextToken();
            if (i == 0 && !content.matches("\\d+")) {
                writer.write("Line " + index + " : The column h020codpays is not an integer.\n");
                hasError = true;
            }
            if (i == 1 && StringUtils.isEmpty(content)) {
                writer.write("Line " + index + " : The column modele is absent.\n");
                hasError = true;
            }
            if (i == 2 && !content.equals("NULL") && !isTypeDecimal(content)) {
                writer.write("Line " + index + " : The column prix_moyen_usine_pays is not an decimal.\n");
                hasError = true;
            }
            if (i == 3 && !content.equals("NULL") && !StringUtils.isEmpty(content) && !isTypeDecimal(content)) {
                writer.write("Line " + index + " : The column cpc_moyen_usine_pays is not an decimal.\n");
                hasError = true;
            }
            hasWrongValue = true;
            i++;
        }
        if (hasWrongValue) {
            writer.write("Line " + index + " : The three column must decimal type.\n");
            hasError = true;
        }
        return hasError;
    }

    /**
     * Verify if all line is empty
     *
     * @param line
     * @param writer
     * @param index
     * @param hasError
     * @return
     * @throws IOException
     */
    static boolean verifyAllLineHasNotEmpty(String line, BufferedWriter writer, int index, boolean hasError) throws IOException {
        StringTokenizer stringTokenizer = new StringTokenizer(line, ";");
        String firstColumn = stringTokenizer.nextToken();
        boolean hasFirstColumnEmpty = false;
        if (line.charAt(0) == ';' || StringUtils.isEmpty(firstColumn)) {
            writer.write("Line " + index + " : The column is has one or many empty\n");
            hasError = true;
            hasFirstColumnEmpty = true;
        }
        boolean hasLineEmpty = false;

        if (!hasFirstColumnEmpty) {
            boolean hastoken = false;
            while (stringTokenizer.hasMoreTokens()) {
                hastoken = true;
                if (StringUtils.isEmpty(stringTokenizer.nextToken())) {

                    hasLineEmpty = true;
                }
            }
            if (!hastoken || hasLineEmpty) {
                writer.write("Line " + index + " : The column is has one or many empty\n");
                hasError = true;
            }
        }
        return hasError;

    }

    /**
     * Verifiy is the column is absent or not the right type
     *
     * @param column
     * @param line
     * @param writer
     * @param index
     * @param hasError
     * @return
     * @throws IOException
     */
    static boolean verifyColumnIsAbsentAndOtherColumnMightTypeDecimalOrNull(String column, String line, BufferedWriter writer, int index, boolean hasError) throws IOException {
        StringTokenizer stringTokenizer = new StringTokenizer(line, ";");
        String modCaisse = stringTokenizer.nextToken();
        if (line.charAt(0) == ';' || StringUtils.isEmpty(modCaisse)) {
            writer.write("Line " + index + " : The column " + column + "  is absent.\n");
            hasError = true;
        }

        // Control the column is a decimal type or no
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            String content = stringTokenizer.nextToken();

            if (i == 0 && !content.equals("NULL") && !isTypeDecimal(content.trim())) {
                writer.write("Line " + index + " : This column lfco2 must be a decimal type.\n");
                hasError = true;
            }
            i++;
        }

        return hasError;
    }

    /**
     * Verify is the column type is a decimal
     *
     * @param value
     * @return
     */
    static boolean isTypeDecimal(String value) {
        return value.matches("-?\\d+(\\.\\d+)?");
    }
}
