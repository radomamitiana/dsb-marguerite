package com.renault.dsb.service.history.dslpr;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.renault.dsb.data.dto.HistoryDslprDTO;
import com.renault.dsb.data.entity.HistoryDslpr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.renault.dsb.data.dto.HistorysDTO;
import com.renault.dsb.mapper.HistoryDslprCauseAleMapper;
import com.renault.dsb.repository.HistoryDslprRepository;

@Service
public class HistoryDslprServiceImpl implements HistoryDslprService {

    @Autowired
    private HistoryDslprRepository historyRepository;
    @Autowired
    private HistoryDslprCauseAleMapper historyFactory;

    /**
     * get story dslpr caus_ale list and filter by date
     */
    @Override
    public HistorysDTO listHistorys(String type, Pageable pageable) {
        HistorysDTO historysDTO = new HistorysDTO();

        Page<HistoryDslpr> historyPage = historyRepository.findByType(type, pageable);
        Page<HistoryDslprDTO> historyDTOPage = historyFactory.historyPageToHistoryDtoPage(historyPage, pageable);

        if (historyDTOPage != null) {
            List<HistoryDslprDTO> historyDslprDTOList = historyDTOPage.getContent().stream()
                    .sorted(Comparator.comparing(HistoryDslprDTO::getCreatedDate).reversed()).collect(Collectors.toList());

            Page<HistoryDslprDTO> historysDTOPage = new PageImpl<>(historyDslprDTOList, historyPage.getPageable(),
                    historyPage.getTotalElements());

            historysDTO.setHistoryDTOS(historysDTOPage);
        }

        return historysDTO;
    }

    /**
     * Save history dslpr file
     */
    @Override
    public HistoryDslprDTO save(HistoryDslprDTO historyDslprDTO) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        historyDslprDTO.setDate(LocalDateTime.now());
        historyDslprDTO.setCreatedDate(dtf.format(historyDslprDTO.getDate()));
        return historyFactory
                .historyToHistoryDTO(historyRepository.save(historyFactory.historyDTOToHistory(historyDslprDTO)));
    }

    /**
     * Search anything in the story dslpr file and filter by date
     */
    @Override
    public HistorysDTO listHistorys(String type, String stringQuery, Pageable pageable) {
        if (StringUtils.isEmpty(stringQuery)) {
            return listHistorys(type, pageable);
        }

        HistorysDTO historysDTO = new HistorysDTO();

        List<HistoryDslpr> historyDslprList = historyRepository
                .findByIpnContainingOrActionContainingOrStateContainingOrCreatedDateContaining(stringQuery, stringQuery,
                        stringQuery, stringQuery);

        List<HistoryDslpr> historyDslprs = historyDslprList.stream().filter(x -> x.getType() != null && x.getType().equals(type))
                .collect(Collectors.toList());
        List<HistoryDslprDTO> historyDslprDTOS = historyFactory.historysToHistorysDTO(historyDslprs);

        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), historyDslprDTOS.size());
        historysDTO.setHistoryDTOS(new PageImpl<>(historyDslprDTOS.subList(start, end), pageable, historyDslprDTOS.size()));

        return historysDTO;
    }

}
