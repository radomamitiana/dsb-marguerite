package com.renault.dsb.service.setting;

import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.data.dto.SettingsDTO;
import com.renault.dsb.data.entity.Setting;
import com.renault.dsb.mapper.SettingMapper;
import com.renault.dsb.repository.SettingRepository;
import com.renault.dsb.service.utils.CryptoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

@Service
public class SettingServiceImpl implements SettingService {

	@Autowired
	private SettingRepository settingRepository;
	@Autowired
	private SettingMapper settingFactory;

	@Override
	public SettingDTO createOrUpdate(SettingDTO settingDTO) {
		List<Setting> settings = settingRepository.findByName(settingDTO.getName());
		SettingDTO settingOldDTO = (settings.size() > 0) ? settingFactory.settingToSettingDTO(settings.get(0)) : null;

		if (settingOldDTO != null && StringUtils.isEmpty(settingDTO.getId()))
			return settingOldDTO;

		if (settingDTO.getIsCrypt()) {
			CryptoUtil cryptoUtil = new CryptoUtil();
			String encryptValue = cryptoUtil.encrypt(settingDTO.getValue());
			if (encryptValue != null)
				settingDTO.setValue(encryptValue);
		}
		return settingFactory
				.settingToSettingDTO(settingRepository.save(settingFactory.settingDTOToSetting(settingDTO)));
	}

	@Override
	public IsDeletedDTO delete(Integer id) {
		IsDeletedDTO isDeletedDTO = new IsDeletedDTO();
		Setting setting = settingRepository.findById(id).orElse(null);
		if (setting != null) {
			settingRepository.delete(setting);
			isDeletedDTO.setIsDeleted(true);
		}
		return isDeletedDTO;
	}

	/**
	 * List setting
	 */
	@Override
	public SettingsDTO listSettings(Pageable pageable) {
		SettingsDTO settingsDTO = new SettingsDTO();

		Page<Setting> settingPage = settingRepository.findAll(pageable);

		Page<SettingDTO> settingDTOPage = settingFactory.settingPageToSettingDtoPage(settingPage, pageable);

		List<SettingDTO> listSettingDTOs = settingDTOPage.getContent().stream().map(this::mapSetting)
				.sorted(Comparator.comparing(SettingDTO::getId).reversed()).collect(Collectors.toList());

		Page<SettingDTO> settingsDTOPage = new PageImpl<>(listSettingDTOs, settingPage.getPageable(),
				settingPage.getTotalElements());

		settingsDTO.setSettingDTOS(settingsDTOPage);

		return settingsDTO;
	}

	/**
	 * Search something in the setting list
	 */
	@Override
	public SettingsDTO listSettings(String stringQuery, Pageable pageable) {
		if (StringUtils.isEmpty(stringQuery)) {
			return listSettings(pageable);
		}

		SettingsDTO settingsDTO = new SettingsDTO();

		List<Setting> settings = settingRepository.findAll();

		List<SettingDTO> settingDTOs = settingFactory.settingsToSettingsDTO(settings);

		BiPredicate<SettingDTO, String> filterSettingPredicate = (settingDTO,
				value) -> (!StringUtils.isEmpty(settingDTO.getName())
						&& settingDTO.getName().toLowerCase().contains(value.toLowerCase()))
						|| (!StringUtils.isEmpty(settingDTO.getValue())
								&& settingDTO.getValue().toLowerCase().contains(value.toLowerCase()));

		List<SettingDTO> listSettingDTOs = settingDTOs.stream().map(this::mapSetting)
				.filter(x -> filterSettingPredicate.test(x, stringQuery))
				.sorted(Comparator.comparing(SettingDTO::getId).reversed()).collect(Collectors.toList());

		int start = (int) pageable.getOffset();
		int end = Math.min((start + pageable.getPageSize()), listSettingDTOs.size());

		settingsDTO
				.setSettingDTOS(new PageImpl<>(listSettingDTOs.subList(start, end), pageable, listSettingDTOs.size()));

		return settingsDTO;
	}

	@Override
	public SettingDTO getValueByName(String name) {
		List<Setting> settings = settingRepository.findByName(name);
		return (settings.size() > 0) ? settingFactory.settingToSettingDTO(settings.get(0)) : null;
	}

	/** Aditional function **/

	/**
	 * Decript value if there is a value crypted
	 * 
	 * @param settingDTO
	 * @return
	 */
	private SettingDTO mapSetting(SettingDTO settingDTO) {
		if (settingDTO.getIsCrypt()) {
			CryptoUtil cryptoUtil = new CryptoUtil();
			String decryptValue = cryptoUtil.decrypt(settingDTO.getValue());
			if (decryptValue != null)
				settingDTO.setValue(decryptValue);
		}
		
		return settingDTO;
	}

}
