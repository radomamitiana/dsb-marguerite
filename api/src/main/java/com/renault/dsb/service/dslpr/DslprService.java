package com.renault.dsb.service.dslpr;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.renault.dsb.data.dto.ResponseDTO;

public interface DslprService {
	
	/**
	 * Download file from locale to the datalake
	 * 
	 * @return
	 */
	Resource download(String ipn, String password, String path, String type, String currentIpn);

	/**
	 * Upload file from local to the datalake
	 * 
	 * @param ipn
	 * @return
	 */
	ResponseDTO upload(String ipn, String password, String path, MultipartFile file, String type, String currentIpn);

	/**
	 * Download file error repport
	 * 
	 * @param errorReport
	 * @return
	 */
	Resource downloadErrorReport(String errorReport);

}
