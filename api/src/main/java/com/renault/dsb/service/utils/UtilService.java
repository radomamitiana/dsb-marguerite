package com.renault.dsb.service.utils;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public interface UtilService {

	final static Logger logger = LoggerFactory.getLogger(UtilService.class);

	final Path rootLocation = Paths.get("storage");

	final Path reportErrorLocation = Paths.get("file");

	/**
	 * Store file on locale storage
	 *
	 * @param file
	 */
	public default void store(MultipartFile file) {
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if (file.isEmpty()) {
				logger.error("Failed to store empty file " + filename);
			}
			if (filename.contains("..")) {
				// This is a security check
				logger.error("Cannot store file with relative path outside current directory " + filename);
			}
			try (InputStream inputStream = file.getInputStream()) {
				Files.copy(inputStream, rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
			}
		} catch (IOException e) {
			logger.error("Failed to store file " + filename, e);
		}
	}

	/**
	 * get file's path
	 *
	 * @param filename
	 * @return
	 */
	public default Path load(String filename) {
		return rootLocation.resolve(filename);
	}

	/**
	 * get file's path error
	 *
	 * @param filename
	 * @return
	 */
	public default Path loadError(String filename) {
		return reportErrorLocation.resolve(filename);
	}

	/**
	 * Load file
	 *
	 * @param filename
	 * @return
	 */
	public default Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				logger.error("Could not read file: " + filename);

			}
		} catch (MalformedURLException e) {
			logger.error("Could not read file: " + filename, e);
		}
		return null;
	}

	/**
	 * Load file error
	 *
	 * @param filename
	 * @return
	 */
	public default Resource loadAsResourceError(String filename) {
		try {
			Path file = loadError(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				logger.error("Could not read file error : " + filename);

			}
		} catch (MalformedURLException e) {
			logger.error("Could not read file error : " + filename, e);
		}
		return null;
	}

	/**
	 * Clear file before uploading the same file
	 *
	 * @param runtime
	 */
	public default void clearData(Runtime runtime, String ipn, String password, String path) {
		try {
			StringBuilder commandLineBuilder = new StringBuilder();
			commandLineBuilder.append("curl -L -k -u ");
			commandLineBuilder.append(ipn);
			commandLineBuilder.append(":");
			commandLineBuilder.append(password);
			commandLineBuilder.append(" -X ");
			commandLineBuilder.append("DELETE ");
			commandLineBuilder.append(path);
			commandLineBuilder.append("?op=DELETE ");

			Process process = runtime.exec(commandLineBuilder.toString());

			String line;
			BufferedReader output = new BufferedReader(new InputStreamReader(process.getInputStream()));

			while ((line = output.readLine()) != null) {

				logger.info(line);

			}
			output.close();
			process.destroy();
			logger.info("Clear successfull");
		} catch (IOException ex) {
			logger.error(ex.getMessage());
		}
	}

	/**
	 * init locale storage to store the file to upload
	 */
	static void initStorage() {
		try {
			if (!Files.exists(rootLocation)) {
				Files.createDirectories(rootLocation);
			}
		} catch (IOException e) {
			logger.error("Could not initialize storage", e);
		}
	}

	/**
	 * init locale storage to store the file report error
	 */
	static void initFileReportError() {
		try {
			if (!Files.exists(reportErrorLocation)) {
				Files.createDirectories(reportErrorLocation);
			}
		} catch (IOException e) {
			logger.error("Could not initialize file", e);
		}
	}

	/**
	 * clear all file to free the space on the disk
	 */
	default void clearAll() {
		if (Files.isDirectory(rootLocation)) {
			File directory = rootLocation.toFile();
			for (File file : directory.listFiles()) {
				if (file.isFile())
					file.delete();
			}
		}
	}

	/**
	 * remove file tmp error reporting
	 * 
	 * @param fileName
	 */
	default void removeFileReportTmp(String fileName) {
		File file = new File(fileName);
		if (file.exists())
			file.delete();
	}
}
