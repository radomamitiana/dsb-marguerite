package com.renault.dsb.service.dslpr;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.StringTokenizer;

import com.renault.dsb.data.constants.DslprType;
import com.renault.dsb.data.dto.HistoryDslprDTO;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.renault.dsb.data.dto.ResponseDTO;
import com.renault.dsb.service.history.dslpr.HistoryDslprService;
import com.renault.dsb.service.utils.UtilService;

@Service
public class DslprServiceImpl implements DslprService, UtilService, VerifyFileDataDslprService {

    private final static Logger logger = LoggerFactory.getLogger(DslprService.class);

    @Autowired
    private HistoryDslprService historyService;

    /**
     * download file from datalake to local
     *
     * @return
     */
    @Override
    public Resource download(String ipn, String password, String path, String type, String currentIpn) {
        clearAll();
        Runtime runtime = Runtime.getRuntime();
        ResponseDTO responseDTO = new ResponseDTO();

        StringBuilder commandLineBuilder = new StringBuilder();
        commandLineBuilder.append("curl -L -k -u ");
        commandLineBuilder.append(ipn);
        commandLineBuilder.append(":");
        commandLineBuilder.append(password);
        commandLineBuilder.append(" -X ");
        commandLineBuilder.append("GET ");
        commandLineBuilder.append(path);
        commandLineBuilder.append("?op=OPEN");

        Process process = null;
        try {
            process = runtime.exec(commandLineBuilder.toString());

            String line;
            BufferedReader output = new BufferedReader(new InputStreamReader(process.getInputStream()));

            BufferedWriter writer = new BufferedWriter(new FileWriter("storage/cause_ale_deviation.csv"));

            while ((line = output.readLine()) != null) {

                writer.write(line + "\n");
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        process.destroy();
        responseDTO.setOK(true);
        logger.info("Download successfull");

        HistoryDslprDTO historyDslprDTO = new HistoryDslprDTO();
        historyDslprDTO.setIpn(currentIpn);
        historyDslprDTO.setAction("Download");
        historyDslprDTO.setState("OK");
        historyDslprDTO.setHasError(false);
        historyDslprDTO.setType(type);
        historyService.save(historyDslprDTO);

        return loadAsResource("cause_ale_deviation.csv");
    }

    /**
     * Upload file from local to the datalake
     *
     * @param ipn
     * @return
     */
    @Override
    public ResponseDTO upload(String ipn, String password, String path, MultipartFile file, String type, String currentIpn) {
        clearAll();
        Runtime runtime = Runtime.getRuntime();
        ResponseDTO responseDTO = new ResponseDTO();

        // Clear the file before uploading the same file
        store(file);
        boolean isOk = verify(type, file.getOriginalFilename(), ipn, currentIpn);
        if (isOk) {
            // if the file control is OK, delete file on datalake and uplaod the new file
            clearData(runtime, ipn, password, path);
            try {
                StringBuilder commandLineBuilder = new StringBuilder();
                commandLineBuilder.append("curl -L -k -u ");
                commandLineBuilder.append(ipn);
                commandLineBuilder.append(":");
                commandLineBuilder.append(password);
                commandLineBuilder.append(" -X ");
                commandLineBuilder.append("PUT ");
                commandLineBuilder.append(path);
                commandLineBuilder.append("?op=create ");
                commandLineBuilder.append("-H 'Content-Type: applicaiton/text' -T ");
                commandLineBuilder.append("storage/" + file.getOriginalFilename());

                Process process = runtime.exec(commandLineBuilder.toString());

                String line;
                BufferedReader output = new BufferedReader(new InputStreamReader(process.getInputStream()));

                while ((line = output.readLine()) != null) {
                    logger.info(line);
                }
                output.close();
                process.destroy();
                responseDTO.setOK(true);
                logger.info("Upload successfull");

            } catch (IOException ex) {
                logger.error(ex.getMessage());
            }
        }
        return responseDTO;

    }

    /**
     * download report file error
     */
    @Override
    public Resource downloadErrorReport(String errorReport) {
        return loadAsResourceError(errorReport);
    }

    /**
     * controle file before to send to the datalake
     *
     * @param fileName
     * @return
     */
    private boolean verify(String type, String fileName, String ipn, String currentIpn) {
        String extension = FilenameUtils.getExtension(fileName);
        int count = 1;
        boolean hasError = false;
        boolean hasSuccess = false;
        StringBuilder stringBuilderErrorFileName = new StringBuilder();

        // Control if is it a text file
        if (!StringUtils.isEmpty(extension) && (extension.equals("csv") || extension.equals("xls")
                || extension.equals("xlsx") || extension.equals("txt"))) {
            File file = new File("storage/" + fileName);
            if (!file.exists())
                return false;

            try {
                FileReader fileReader = new FileReader("storage/" + fileName);
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                int index = 2;
                int day = LocalDateTime.now().getDayOfMonth();
                int month = LocalDateTime.now().getMonthValue();
                int year = LocalDateTime.now().getYear();
                int hour = LocalDateTime.now().getHour();
                int minute = LocalDateTime.now().getMinute();
                int second = LocalDateTime.now().getSecond();

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

                stringBuilderErrorFileName.append("error_report_" + day);
                stringBuilderErrorFileName.append("-" + month);
                stringBuilderErrorFileName.append("-" + year);
                stringBuilderErrorFileName.append("_" + hour);
                stringBuilderErrorFileName.append("_" + minute);
                stringBuilderErrorFileName.append("_" + second);
                stringBuilderErrorFileName.append(".txt");

                // Build the file error report
                BufferedWriter writer = new BufferedWriter(
                        new FileWriter("file/" + stringBuilderErrorFileName.toString()));
                StringBuilder stringBuilderReportTitle = new StringBuilder();
                stringBuilderReportTitle.append("Action : Upload file\n\n");
                stringBuilderReportTitle.append("File name : ");
                stringBuilderReportTitle.append(fileName + "\n\n");
                stringBuilderReportTitle.append("Date : ");
                stringBuilderReportTitle.append(dtf.format(LocalDateTime.now()).toString() + "\n\n");
                stringBuilderReportTitle.append("User : ");

                stringBuilderReportTitle.append(currentIpn);
                writer.write(stringBuilderReportTitle.toString() + "\n\n");
                writer.write("Result of upload : Errors found  - File rejected\n\n");
                writer.write("Errors :\n");
                String line = null;
                line = bufferedReader.readLine();
                StringTokenizer stringTokenizerFirstLine = new StringTokenizer(line, ";");

                switch (type) {
                    case DslprType
                            .CAUSE_ALE:
                        hasError = verifyColumn(12, stringTokenizerFirstLine, writer, hasError, index);
                        break;
                    case DslprType
                            .CORRESP_ISO_CODEPAYS:
                        hasError = verifyColumn(2, stringTokenizerFirstLine, writer, hasError, index);
                        break;
                    case DslprType
                            .CORRESPONDANCES:
                        hasError = verifyColumn(4, stringTokenizerFirstLine, writer, hasError, index);
                        break;
                    case DslprType
                            .FACTOR_CAISSE:
                        hasError = verifyColumn(4, stringTokenizerFirstLine, writer, hasError, index);
                        break;
                    case DslprType
                            .FACTOR_COMMERCE:
                        hasError = verifyColumn(4, stringTokenizerFirstLine, writer, hasError, index);
                        break;
                    case DslprType
                            .PRIX_MOYEN:
                        hasError = verifyColumn(5, stringTokenizerFirstLine, writer, hasError, index);
                        break;
                    default:
                        break;
                }

                // Control the obligatory and type of column
                while (line != null) {
                    line = bufferedReader.readLine();
                    if (line == null || StringUtils.isEmpty(line))
                        break;

                    switch (type) {
                        case DslprType
                                .CAUSE_ALE:
                            hasError = verifyFileCauseAle(line, writer, index, hasError);
                            break;
                        case DslprType
                                .CORRESP_ISO_CODEPAYS:
                            hasError = verifyFileCodePays(line, writer, index, hasError);
                            break;
                        case DslprType
                                .CORRESPONDANCES:
                            hasError = verifyFileTableCorrespondances(line, writer, index, hasError);
                            break;
                        case DslprType
                                .FACTOR_CAISSE:
                            hasError = verifyFileLoadFactorCaisse(line, writer, index, hasError);
                            break;
                        case DslprType
                                .FACTOR_COMMERCE:
                            hasError = verifyFileFactorCommerce(line, writer, index, hasError);
                            break;
                        case DslprType
                                .PRIX_MOYEN:
                            hasError = verifyFilePrixMoyen(line, writer, index, hasError);
                            break;
                        default:
                            break;
                    }


                    // next line
                    if (!hasError)
                        hasSuccess = true;
                    index++;
                    count++;
                }
                writer.close();
                fileReader.close();
            } catch (IOException e) {
                System.err.println("Error -- " + e.toString());
            }
        } else {
            try {

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                stringBuilderErrorFileName.append("error_file_extension");
                stringBuilderErrorFileName.append(".txt");

                BufferedWriter writer = new BufferedWriter(
                        new FileWriter("file/" + stringBuilderErrorFileName.toString()));
                StringBuilder stringBuilderReportTitle = new StringBuilder();
                stringBuilderReportTitle.append("Action : Upload file\n\n");
                stringBuilderReportTitle.append("File name : ");
                stringBuilderReportTitle.append(fileName + "\n\n");
                stringBuilderReportTitle.append("Date : ");
                stringBuilderReportTitle.append(dtf.format(LocalDateTime.now()).toString() + "\n\n");
                stringBuilderReportTitle.append("User : ");
                stringBuilderReportTitle.append(currentIpn);
                writer.write(stringBuilderReportTitle.toString() + "\n\n");
                writer.write("Result of upload : Errors found  - File rejected\n\n");
                writer.write("Errors :\n");
                writer.write("File extension error. This is not a text file");
                writer.close();
                hasError = true;

            } catch (IOException e) {
                System.err.println("Error -- " + e.toString());
            }

        }

        // Insert a new row in the history table
        HistoryDslprDTO historyDslprDTO = new HistoryDslprDTO();
        historyDslprDTO.setType(type);
        historyDslprDTO.setIpn(currentIpn);
        historyDslprDTO.setAction("Upload");
        historyDslprDTO.setSizeAddLines(count);
        if (!hasError && hasSuccess) {
            historyDslprDTO.setHasError(false);
            historyDslprDTO.setState("OK");

            // save history to the database
            historyService.save(historyDslprDTO);
            removeFileReportTmp("file/" + stringBuilderErrorFileName.toString());
            return true;
        }
        historyDslprDTO.setHasError(true);
        historyDslprDTO.setErrorReport(stringBuilderErrorFileName.toString());
        historyDslprDTO.setState("KO");

        // save history to the database
        historyService.save(historyDslprDTO);
        return false;
    }

}
