package com.renault.dsb.service.user;

import com.renault.dsb.mapper.UserMapper;
import com.renault.dsb.mapper.RoleMapper;
import com.renault.dsb.data.dto.*;
import com.renault.dsb.data.entity.Role;
import com.renault.dsb.data.entity.RoleName;
import com.renault.dsb.data.entity.User;
import com.renault.dsb.repository.RoleRepository;
import com.renault.dsb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userFactory;

    @Autowired
    private RoleMapper roleFactory;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserDTO create(UserDTO userDTO) {
        if (!StringUtils.isEmpty(userDTO.getIpn())) {
            List<User> users = userRepository.findByIpn(userDTO.getIpn());
            User user = (users.size() > 0) ? users.get(0) : null;
            if (user != null)
                return userFactory.userToUserDTO(user);
        }
        return createOrUpdate(userDTO);
    }

    @Override
    public UserDTO update(UserDTO userDTO) {
        if (!StringUtils.isEmpty(userDTO.getId()) && userDTO.getDropdownDataDTOS() != null
                && !userDTO.getDropdownDataDTOS().isEmpty() && this.findById(userDTO.getId()) != null) {
            Set<String> roles = new HashSet<>();
            userDTO.getDropdownDataDTOS().forEach(dropdownDataDTO -> roles.add(dropdownDataDTO.getValue()));
            UserDTO userUpdatedDTO = this.findById(userDTO.getId());
            userUpdatedDTO.setStrRoles(roles);
            return createOrUpdate(userUpdatedDTO);
        }
        return null;
    }

    @Override
    public IsDeletedDTO deleteUser(Integer id) {
        IsDeletedDTO isDeletedDTO = new IsDeletedDTO();
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            userRepository.delete(user);
            isDeletedDTO.setIsDeleted(true);
        }
        return isDeletedDTO;
    }

    @Override
    public UsersDTO listUsers(Pageable pageable) {
        UsersDTO usersDTO = new UsersDTO();

        Page<User> userPage = userRepository.findAll(pageable);

        Page<UserDTO> userDTOPages = userFactory.userPageToUserDtoPage(userPage, pageable);

        List<UserDTO> listUserDTOs = userDTOPages.getContent().stream().map(this::mapUser)
                .sorted(Comparator.comparing(UserDTO::getId).reversed()).collect(Collectors.toList());

        Page<UserDTO> userDTOPage = new PageImpl<>(listUserDTOs, userPage.getPageable(), userPage.getTotalElements());

        usersDTO.setUserDTOs(userDTOPage);

        return usersDTO;
    }

    /**
     * search anything in the user
     */
    @Override
    public UsersDTO listUsers(String stringQuery, Pageable pageable) {
        if (StringUtils.isEmpty(stringQuery))
            return listUsers(pageable);

        UsersDTO usersDTO = new UsersDTO();

        List<User> users = userRepository.findAll();

        List<UserDTO> userDTOS = userFactory.usersToUsersDTO(users);

        BiPredicate<UserDTO, String> filterUserPredicate = (userDTO,
                                                            value) -> (!StringUtils.isEmpty(userDTO.getIpn())
                && userDTO.getIpn().toLowerCase().contains(value.toLowerCase()))
                || ((userDTO.getStrRoles() != null && userDTO.getStrRoles().size() > 0) && userDTO.getStrRoles()
                .stream().anyMatch(role -> role.toLowerCase().contains(value.toLowerCase())));

        List<UserDTO> listUserDTOS = userDTOS.stream().map(this::mapUser)
                .filter(userDTO -> filterUserPredicate.test(userDTO, stringQuery))
                .sorted(Comparator.comparing(UserDTO::getId).reversed()).collect(Collectors.toList());

        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), listUserDTOS.size());

        usersDTO.setUserDTOs(new PageImpl<>(listUserDTOS.subList(start, end), pageable, listUserDTOS.size()));

        return usersDTO;
    }

    @Override
    public UserDTO findById(Integer id) {
        return userFactory.userToUserDTO(userRepository.findById(id).orElse(null));
    }

    @Override
    public AccountInformationDTO getAccountInformationByIpn(String ipn) {
        System.out.print("IPN : " + ipn);
        User user = userRepository.findByIpn(ipn).size() > 0 ? userRepository.findByIpn(ipn).get(0) : null;
        if (user != null && user.getRoles() != null && !user.getRoles().isEmpty()) {
            AccountInformationDTO accountInformationDTO = new AccountInformationDTO();
            List<String> roles = new ArrayList<>();
            user.getRoles().forEach(role -> {
                String roleName = role.getName().name().replace("ROLE_", "");
                roles.add(roleName);
            });
            accountInformationDTO.setRoles(roles);
            accountInformationDTO.setIsAdmin(roles.contains("ADMIN"));
            return accountInformationDTO;
        }
        return null;
    }

    @Override
    public void initUser() {
        if (userRepository.findByIpn("p102818").isEmpty()) {
            UserDTO userDTO = new UserDTO();
            userDTO.setIpn("p102818");
            Set<String> roles = new HashSet<>();
            roles.add("ADMIN");
            userDTO.setStrRoles(roles);
            create(userDTO);
        }
    }


    /** Additional function **/
    /**
     * Create or update user
     *
     * @param userDTO
     * @return
     */
    private UserDTO createOrUpdate(UserDTO userDTO) {
        Set<String> strRoles = userDTO.getStrRoles();

        if (strRoles != null && !strRoles.isEmpty()) {
            Set<Role> roles = new HashSet<>();
            strRoles.forEach(role -> {
                switch (role) {
                    case "ADMIN":
                        RoleDTO adminRole = roleService.findByName(RoleName.ROLE_ADMIN);
                        roles.add(roleFactory.roleDTOToRole(adminRole));
                        break;
                    case "CO2":
                        RoleDTO co2Role = roleService.findByName(RoleName.ROLE_CO2);
                        roles.add(roleFactory.roleDTOToRole(co2Role));
                        break;
                    case "TOP_ALLIANCE":
                        RoleDTO topAllianceRole = roleService.findByName(RoleName.ROLE_TOP_ALLIANCE);
                        roles.add(roleFactory.roleDTOToRole(topAllianceRole));
                        break;
                    case "DSLPR":
                        RoleDTO dslprRole = roleService.findByName(RoleName.ROLE_DSLPR);
                        roles.add(roleFactory.roleDTOToRole(dslprRole));
                        break;
                    case "MONITORING":
                        RoleDTO monitoringRole = roleService.findByName(RoleName.ROLE_MONITORING);
                        roles.add(roleFactory.roleDTOToRole(monitoringRole));
                        break;

                }
            });
            if (!roles.isEmpty()) {
                User user = userFactory.userDTOToUser(userDTO);

                if (user != null) {
                    user.setRoles(roles);
                    return userFactory.userToUserDTO(userRepository.save(user));
                }
            }
        }
        return null;
    }

    /**
     * Encrypt the setting content
     *
     * @param userDTO
     * @return
     */
    private UserDTO mapUser(UserDTO userDTO) {
        User user = userRepository.findById(userDTO.getId()).orElse(null);
        Set<String> strRoles = new HashSet<>();
        List<DropdownDataDTO> dropdownDataDTOS = new ArrayList<>();
        if (user != null) {
            user.getRoles().forEach(role -> {
                String roleName = role.getName().name().replace("ROLE_", "");
                strRoles.add(roleName);
                DropdownDataDTO dropdownDataDTO = new DropdownDataDTO();
                dropdownDataDTO.setId(role.getId());
                dropdownDataDTO.setValue(roleName);
                dropdownDataDTOS.add(dropdownDataDTO);
            });
            DropdownListDTO dropdownListDTO = new DropdownListDTO();
            dropdownListDTO.setDropdownDataDTOS(dropdownDataDTOS);
            userDTO.setDropdownListDTO(dropdownListDTO);
            userDTO.setStrRoles(strRoles);
        }
        return userDTO;
    }

}
