package com.renault.dsb.service.history.dslpr;

import org.springframework.data.domain.Pageable;

import com.renault.dsb.data.dto.HistoryDslprDTO;
import com.renault.dsb.data.dto.HistorysDTO;

public interface HistoryDslprService {
	HistorysDTO listHistorys(String type, Pageable pageable);

	HistoryDslprDTO save(HistoryDslprDTO historyDslprDTO);

	HistorysDTO listHistorys(String type, String stringQuery, Pageable pageable);
}
