package com.renault.dsb.mapper;

import java.util.List;

import com.renault.dsb.data.dto.HistoryDslprDTO;
import com.renault.dsb.data.entity.HistoryDslpr;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@Mapper
public interface HistoryDslprCauseAleMapper {
	
	HistoryDslprDTO historyToHistoryDTO(HistoryDslpr history);

	HistoryDslpr historyDTOToHistory(HistoryDslprDTO historyDslprDTO);

	List<HistoryDslprDTO> historysToHistorysDTO(List<HistoryDslpr> historys);

	List<HistoryDslpr> historysDTOToHistorys(List<HistoryDslprDTO> historysDTO);

	default Page<HistoryDslprDTO> historyPageToHistoryDtoPage(Page<HistoryDslpr> historyPage, Pageable pageable) {
		List<HistoryDslprDTO> historyDslprDtos = historysToHistorysDTO(historyPage.getContent());
		Page<HistoryDslprDTO> historyDTOPage = new PageImpl<>(historyDslprDtos, pageable, historyPage.getTotalElements());

		return historyDTOPage;
	}

}
