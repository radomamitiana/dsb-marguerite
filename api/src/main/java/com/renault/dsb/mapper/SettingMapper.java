package com.renault.dsb.mapper;

import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.data.entity.Setting;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Mapper
public interface SettingMapper {
	SettingDTO settingToSettingDTO(Setting setting);

	Setting settingDTOToSetting(SettingDTO settingDTO);

	List<SettingDTO> settingsToSettingsDTO(List<Setting> settings);

	List<Setting> settingsDTOToSettings(List<SettingDTO> settingsDTO);

	default Page<SettingDTO> settingPageToSettingDtoPage(Page<Setting> settingPage, Pageable pageable) {
		List<SettingDTO> settingDtos = settingsToSettingsDTO(settingPage.getContent());
		Page<SettingDTO> settingDTOPage = new PageImpl<>(settingDtos, pageable, settingPage.getTotalElements());

		return settingDTOPage;
	}

}
