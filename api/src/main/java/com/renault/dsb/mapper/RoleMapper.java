package com.renault.dsb.mapper;

import com.renault.dsb.data.dto.RoleDTO;
import com.renault.dsb.data.entity.Role;

import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper {
	RoleDTO roleToRoleDTO(Role role);
	Role roleDTOToRole(RoleDTO roleDTO);
	List <RoleDTO> rolesToRolesDTO(List<Role> roles);
	List <Role> rolesDTOToRoles(List<RoleDTO> rolesDTO);
}