package com.renault.dsb.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.renault.dsb.data.dto.HistoryDTO;
import com.renault.dsb.repository.HistoryDslprCauseAleRepository;
import com.renault.dsb.service.history.dslpr.cause_ale.HistoryDslprCauseAleService;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class HistoryDslprCauseAleTest {

	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private HistoryDslprCauseAleService historyService;

	@Autowired
	private HistoryDslprCauseAleRepository historyRepository;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}


	/**
	 * Test search endpoint
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_searchHistorys() throws Exception {

		HistoryDTO historyDownloadDTO = new HistoryDTO();
		historyDownloadDTO.setIpn("mock.mvc.testing.ipn.0007");
		historyDownloadDTO.setAction("mock.mvc.org.action.testing.VHv1BumKQ8.Download");
		historyDownloadDTO.setSizeAddLines(22);
		historyDownloadDTO.setHasError(false);
		historyDownloadDTO.setState("OK");

		historyDownloadDTO = historyService.save(historyDownloadDTO);

		Assert.assertNotNull(historyDownloadDTO.getId());

		HistoryDTO historyUploadDTO = new HistoryDTO();
		historyUploadDTO.setIpn("mock.mvc.testing.ipn.0007");
		historyUploadDTO.setAction("mock.mvc.org.action.testing.VHv1BumKQ8.Upload");
		historyUploadDTO.setSizeAddLines(22);
		historyUploadDTO.setHasError(false);
		historyUploadDTO.setState("OK");

		historyUploadDTO = historyService.save(historyUploadDTO);

		Assert.assertNotNull(historyUploadDTO.getId());

		MockHttpServletRequestBuilder requestBuilder = get(
				"/api/history/dslpr/cause_ale/list?stringQuery=mock.mvc.org.action.testing.VHv1BumKQ8")
						.contentType(MediaType.APPLICATION_JSON);

		MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
		String resultString = resultPost.getResponse().getContentAsString();
		ObjectNode node = objectMapper.readValue(resultString, ObjectNode.class);
		String result = node.get("historyDTOS").get("content").toString();
		List<HistoryDTO> lstResult = objectMapper.readValue(result, new TypeReference<List<HistoryDTO>>() {
		});

		Assert.assertNotEquals(lstResult.size(), 0);

		// delete history after test
		lstResult.forEach(dto -> historyRepository.deleteById(dto.getId()));
	}

	/**
	 * test list story endpoint
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_listHistorys() throws Exception {

		HistoryDTO historyDownloadDTO = new HistoryDTO();
		historyDownloadDTO.setIpn("mock.mvc.testing.ipn.0007");
		historyDownloadDTO.setAction("Download");
		historyDownloadDTO.setSizeAddLines(22);
		historyDownloadDTO.setHasError(false);
		historyDownloadDTO.setState("OK");

		historyDownloadDTO = historyService.save(historyDownloadDTO);

		Assert.assertNotNull(historyDownloadDTO.getId());

		HistoryDTO historyUploadDTO = new HistoryDTO();
		historyUploadDTO.setIpn("mock.mvc.testing.ipn.0007");
		historyUploadDTO.setAction("Upload");
		historyUploadDTO.setSizeAddLines(22);
		historyUploadDTO.setHasError(false);
		historyUploadDTO.setState("OK");

		historyUploadDTO = historyService.save(historyUploadDTO);

		Assert.assertNotNull(historyUploadDTO.getId());

		MockHttpServletRequestBuilder requestBuilder = get("/api/history/dslpr/cause_ale/list")
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
		String resultString = resultPost.getResponse().getContentAsString();
		ObjectNode node = objectMapper.readValue(resultString, ObjectNode.class);
		String result = node.get("historyDTOS").get("content").toString();
		List<HistoryDTO> lstResult = objectMapper.readValue(result, new TypeReference<List<HistoryDTO>>() {
		});

		lstResult.forEach(dto -> Assert.assertNotNull(dto.getId()));

		// delete history after test
		lstResult.forEach(dto -> historyRepository.deleteById(dto.getId()));
	}
}
