package com.renault.dsb.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.data.entity.Setting;
import com.renault.dsb.repository.SettingRepository;
import com.renault.dsb.service.setting.SettingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class SettingControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private SettingService settingService;

	@Autowired
	private SettingRepository settingrepository;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	/**
	 * test create setting endpoint
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_createOrUpdate() throws Exception {

		SettingDTO settingDTO = new SettingDTO();
		settingDTO.setName("mock.mvc.hibernate.dialect.testing.VHv1BumKQ8");
		settingDTO.setValue("mock.mvc.org.hibernate.dialect.H2Dialect.testing.VHv1BumKQ8");
		settingDTO.setIsCrypt(false);

		String contentSetting = objectMapper.writeValueAsString(settingDTO);

		MockHttpServletRequestBuilder requestBuilder = post("/api/setting/createOrUpdate")
				.contentType(MediaType.APPLICATION_JSON).content(contentSetting);

		MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
		String resultString = resultPost.getResponse().getContentAsString();
		SettingDTO result = objectMapper.readValue(resultString, SettingDTO.class);
		Assert.assertNotNull(result.getId());

		// delete setting after creation
		List<Setting> settings = settingrepository.findByName(settingDTO.getName());
		settings.forEach(setting -> settingrepository.deleteById(setting.getId()));
	}

	/**
	 * test search setting endpoint
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_searchSettings() throws Exception {

		SettingDTO settingDTO = new SettingDTO();
		settingDTO.setName("mock.mvc.hibernate.dialect.testing.VHv1BumKQ8");
		settingDTO.setValue("mock.mvc.org.hibernate.dialect.H2Dialect.testing.VHv1BumKQ8");
		settingDTO.setIsCrypt(false);

		settingDTO = settingService.createOrUpdate(settingDTO);

		Assert.assertNotNull(settingDTO.getId());

		MockHttpServletRequestBuilder requestBuilder = get("/api/setting/list?stringQuery=" + settingDTO.getValue())
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
		String resultString = resultPost.getResponse().getContentAsString();
		ObjectNode node = objectMapper.readValue(resultString, ObjectNode.class);
		String result = node.get("settingDTOS").get("content").toString();
		List<SettingDTO> lstResult = objectMapper.readValue(result, new TypeReference<List<SettingDTO>>() {
		});

		Assert.assertEquals(lstResult.size(), 1);

		// delete setting after creation
		lstResult.forEach(setting -> settingService.delete(setting.getId()));
	}

	/**
	 * test list setting endpoint
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_listSettings() throws Exception {

		SettingDTO settingDTO = new SettingDTO();
		settingDTO.setName("mock.mvc.hibernate.dialect.testing.VHv1BumKQ8");
		settingDTO.setValue("mock.mvc.org.hibernate.dialect.H2Dialect.testing.VHv1BumKQ8");
		settingDTO.setIsCrypt(false);

		settingDTO = settingService.createOrUpdate(settingDTO);

		Assert.assertNotNull(settingDTO.getId());

		MockHttpServletRequestBuilder requestBuilder = get("/api/setting/list?stringQuery=" + settingDTO.getValue())
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
		String resultString = resultPost.getResponse().getContentAsString();
		ObjectNode node = objectMapper.readValue(resultString, ObjectNode.class);
		String result = node.get("settingDTOS").get("content").toString();
		List<SettingDTO> lstResult = objectMapper.readValue(result, new TypeReference<List<SettingDTO>>() {
		});

		lstResult.forEach(dto -> Assert.assertNotNull(dto.getId()));

		// delete setting after creation
		lstResult.forEach(setting -> settingService.delete(setting.getId()));
	}

	/**
	 * test delete setting endpoint
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_delete() throws Exception {

		SettingDTO settingDTO = new SettingDTO();
		settingDTO.setName("mock.mvc.hibernate.dialect.testing.VHv1BumKQ8");
		settingDTO.setValue("mock.mvc.org.hibernate.dialect.H2Dialect.testing.VHv1BumKQ8");
		settingDTO.setIsCrypt(false);

		settingDTO = settingService.createOrUpdate(settingDTO);

		Assert.assertNotNull(settingDTO.getId());

		MockHttpServletRequestBuilder requestBuilder = delete("/api/setting/delete?id=" + settingDTO.getId())
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
		String resultString = resultPost.getResponse().getContentAsString();
		IsDeletedDTO result = objectMapper.readValue(resultString, IsDeletedDTO.class);
		Assert.assertEquals(result.getIsDeleted(), true);

		// delete setting after creation
		List<Setting> settings = settingrepository.findByName(settingDTO.getName());
		settings.forEach(setting -> settingrepository.deleteById(setting.getId()));
	}

}
