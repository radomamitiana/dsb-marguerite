package com.renault.dsb.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.renault.dsb.data.dto.*;
import com.renault.dsb.data.entity.User;
import com.renault.dsb.repository.UserRepository;
import com.renault.dsb.service.user.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class UserControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    /**
     * test add user endpoint
     *
     * @throws Exception
     */
    @Test
    public void test_add() throws Exception {

        Set<String> strRoles = new HashSet<>();
        strRoles.add("CO2");
        strRoles.add("TOP_ALLIANCE");
        strRoles.add("MONITORING");
        strRoles.add("DSLPR");

        UserDTO userDTO = new UserDTO();
        userDTO.setIpn("mock.mvc.application.testing.VHv1BumKQ8.ipn");
        userDTO.setStrRoles(strRoles);
        String contentUser = objectMapper.writeValueAsString(userDTO);

        MockHttpServletRequestBuilder requestBuilder = post("/api/user/add").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(contentUser);

        MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
        String resultString = resultPost.getResponse().getContentAsString();
        UserDTO result = objectMapper.readValue(resultString, UserDTO.class);
        Assert.assertNotNull(result.getId());

        // delete user after test
        List<User> users = userRepository.findByIpn(userDTO.getIpn());
        users.forEach(user -> userRepository.deleteById(user.getId()));
    }

    /**
     * test update user endpoint
     *
     * @throws Exception
     */
    @Test
    public void test_update() throws Exception {

        Set<String> strRoles = new HashSet<>();
        strRoles.add("CO2");
        strRoles.add("TOP_ALLIANCE");
        strRoles.add("MONITORING");
        strRoles.add("DSLPR");

        UserDTO userDTO = new UserDTO();
        userDTO.setIpn("mock.mvc.application.testing.VHv1BumKQ8.ipn");
        userDTO.setStrRoles(strRoles);

        userDTO = userService.create(userDTO);

        Assert.assertNotNull(userDTO.getId());
        DropdownDataDTO dropdownDataCO2DTO = new DropdownDataDTO();
        dropdownDataCO2DTO.setId(1);
        dropdownDataCO2DTO.setValue("CO2");

        DropdownDataDTO dropdownDataDSLPRDTO = new DropdownDataDTO();
        dropdownDataDSLPRDTO.setId(2);
        dropdownDataDSLPRDTO.setValue("DSLPR");

        List<DropdownDataDTO> dropdownDataDTOS = new ArrayList<>();
        dropdownDataDTOS.add(dropdownDataCO2DTO);
        dropdownDataDTOS.add(dropdownDataDSLPRDTO);
        userDTO.setDropdownDataDTOS(dropdownDataDTOS);

        String contentUser = objectMapper.writeValueAsString(userDTO);

        MockHttpServletRequestBuilder requestBuilder = put("/api/user/update").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(contentUser);

        MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
        String resultString = resultPost.getResponse().getContentAsString();
        UserDTO result = objectMapper.readValue(resultString, UserDTO.class);
        Assert.assertNotNull(result.getId());

        // delete user after test
        List<User> users = userRepository.findByIpn(userDTO.getIpn());
        users.forEach(user -> userRepository.deleteById(user.getId()));
    }

    /**
     * test search user endpoint
     *
     * @throws Exception
     */
    @Test
    public void test_searchUsers() throws Exception {

        Set<String> strRoles = new HashSet<>();
        strRoles.add("CO2");
        strRoles.add("TOP_ALLIANCE");
        strRoles.add("MONITORING");
        strRoles.add("DSLPR");

        UserDTO userDTO = new UserDTO();
        userDTO.setIpn("mock.mvc.application.testing.VHv1BumKQ8.ipn");
        userDTO.setStrRoles(strRoles);

        userDTO = userService.create(userDTO);

        Assert.assertNotNull(userDTO.getId());

        MockHttpServletRequestBuilder requestBuilder = get("/api/user/list?stringQuery=" + userDTO.getIpn())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
        String resultString = resultPost.getResponse().getContentAsString();
        ObjectNode node = objectMapper.readValue(resultString, ObjectNode.class);
        String result = node.get("userDTOs").get("content").toString();
        List<UserDTO> lstResult = objectMapper.readValue(result, new TypeReference<List<UserDTO>>() {
        });

        Assert.assertNotEquals(lstResult.size(), 0);

        // delete user after test
        List<User> users = userRepository.findByIpn(userDTO.getIpn());
        users.forEach(user -> userRepository.deleteById(user.getId()));
    }

    /**
     * test list users endpoint
     *
     * @throws Exception
     */
    @Test
    public void test_listUsers() throws Exception {

        Set<String> strRoles = new HashSet<>();
        strRoles.add("CO2");
        strRoles.add("TOP_ALLIANCE");
        strRoles.add("MONITORING");
        strRoles.add("DSLPR");

        UserDTO userDTO = new UserDTO();
        userDTO.setIpn("mock.mvc.application.testing.VHv1BumKQ8.ipn");
        userDTO.setStrRoles(strRoles);

        userDTO = userService.create(userDTO);

        Assert.assertNotNull(userDTO.getId());

        MockHttpServletRequestBuilder requestBuilder = get("/api/user/list").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
        String resultString = resultPost.getResponse().getContentAsString();
        ObjectNode node = objectMapper.readValue(resultString, ObjectNode.class);
        String result = node.get("userDTOs").get("content").toString();
        List<UserDTO> lstResult = objectMapper.readValue(result, new TypeReference<List<UserDTO>>() {
        });

        lstResult.forEach(dto -> Assert.assertNotNull(dto.getId()));

        // delete user after test
        List<User> users = userRepository.findByIpn(userDTO.getIpn());
        users.forEach(user -> userRepository.deleteById(user.getId()));
    }

    /**
     * test delete user endpoint
     *
     * @throws Exception
     */
    @Test
    public void test_delete() throws Exception {

        Set<String> strRoles = new HashSet<>();
        strRoles.add("CO2");
        strRoles.add("TOP_ALLIANCE");
        strRoles.add("MONITORING");
        strRoles.add("DSLPR");

        UserDTO userDTO = new UserDTO();
        userDTO.setIpn("mock.mvc.application.testing.VHv1BumKQ8.ipn");
        userDTO.setStrRoles(strRoles);

        userDTO = userService.create(userDTO);

        Assert.assertNotNull(userDTO.getId());

        MockHttpServletRequestBuilder requestBuilder = delete("/api/user/delete?id=" + userDTO.getId())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult resultPost = mockMvc.perform(requestBuilder).andReturn();
        String resultString = resultPost.getResponse().getContentAsString();
        IsDeletedDTO result = objectMapper.readValue(resultString, IsDeletedDTO.class);
        Assert.assertEquals(result.getIsDeleted(), true);

        // delete user after test
        List<User> users = userRepository.findByIpn(userDTO.getIpn());
        users.forEach(user -> userRepository.deleteById(user.getId()));

    }

}
