import { AuthConfig } from 'angular-oauth2-oidc';

export const environment = {
  production: false,
  apiUrl: '/api',

  issuers: {
    'dok-dev': 'https://idp2.renault.com/nidp/oauth/nam', // development
    'dok-re7': 'https://idp.renault.com/nidp/oauth/nam', // re7
    'ope': 'https://idp.renault.com/nidp/oauth/nam', // production environment,
  },
  loginUrls: {
    'dok-dev': 'https://idp2.renault.com/nidp/oauth/nam/authz', // development
    'dok-re7': 'https://idp.renault.com/nidp/oauth/nam/authz', // re7
    'ope': 'https://idp.renault.com/nidp/oauth/nam/authz', // production environment,
  },
  logoutUrls: {
    'dok-dev': 'https://idp2.renault.com/nidp/app/logout', // development
    'dok-re7': 'https://idp.renault.com/nidp/app/logout', // re7
    'ope': 'https://idp.renault.com/nidp/app/logout', // production environment,
  },
  clientsIds: {
    'dok-dev': '2179f5ac-a1a7-4d15-a1ab-304418730b10', // development
    'dok-re7': 'TODO', // re7
    'ope': 'TODO', // production environment,
  },
  redirectUris: {
    'local': 'https://localhost:4200/index.html',
    'dok-dev': 'https://dsb-app.dev1.applis.renault.fr/index.html', // development
    'dok-re7': 'TODO', // re7    
    'ope': 'TODO',  // production environment

  },
  userInfoEndpoints: {
    'dok-dev': 'https://idp2.renault.com/nidp/oauth/nam/userinfo', // development
    'dok-re7': 'https://idp.renault.com/nidp/oauth/nam/userinfo', // re7
    'ope': 'https://idp.renault.com/nidp/oauth/nam/userinfo' // production environment

  },
  allowedUrls: ['http://localhost:8080','https://dsb-api.dev1.applis.renault.fr', 'https://dsb-api.re7.applis.renault.fr', 'https://dsb-api.ope.applis.renault.fr'],

  apiUrls: {
    'dok-dev': 'https://dsb-api.dev1.applis.renault.fr', // development
    'dok-re7': 'https://dsb-api.dev1.applis.renault.fr', // re7
    'ope': 'https://dsb-api.dev1.applis.renault.fr' // production environment,
  },


};

