import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
    issuer: 'https://idp2.renault.com/nidp/oauth/nam',
    loginUrl: "https://idp2.renault.com/nidp/oauth/nam/authz",
    logoutUrl: "https://idp2.renault.com/nidp/app/logout",
    clientId: '2179f5ac-a1a7-4d15-a1ab-304418730b10',
    scope: "openid arca role-dsb-irn70295",
    redirectUri: "https://localhost:4200/index.html",
    showDebugInformation: true,

}