export class User {
  constructor(
    public id: number,
    public ipn: string,
    public strRoles: [],
    public dropdownListDTO: any
  ) {
  }
}
