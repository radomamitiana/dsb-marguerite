export class History {
    constructor(
      public id: number,
      public ipn: string,
      public action: string,
      public state: string,
      public createdDate: string,
      public sizeAddLines: number,
      public errorReport: string,
      public hasError: boolean
    ) {
    }
  }
