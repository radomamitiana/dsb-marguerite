export class DslprType {
    public static  CAUSE_ALE = 'cause_ale';
    public static  FACTOR_CAISSE = 'factor_caisse';
    public static  FACTOR_COMMERCE = 'factor_commerce';
    public static  CORRESP_ISO_CODEPAYS = 'corresp_iso_codepays';
    public static  CORRESPONDANCES = 'correspondances';
    public static  PRIX_MOYEN = 'prix_moyen';
  }
  