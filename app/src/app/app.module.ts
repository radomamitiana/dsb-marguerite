import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/* Begin Font awesome library */
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);
/* End Font awesome library */

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown-angular7';
import { FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppBootstrapModule } from './app-bootstrap/app-bootstrap.module';
import { GestionUserComponent } from './gestion-user/gestion-user.component';
import { AccueilComponent } from './accueil/accueil.component';
import { HomeComponent } from './home/home.component';
import { LogoutComponent } from './logout/logout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DslprComponent } from './dslpr/dslpr.component';
import { Co2Component } from './co2/co2.component';
import { TopAllianceComponent } from './top-alliance/top-alliance.component';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { ParametreComponent } from './parametre/parametre.component';

// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { OAuthModule, OAuthService } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';
import { GetService } from './service/get.service';

@NgModule({
  declarations: [
    AppComponent,
    GestionUserComponent,
    AccueilComponent,
    HomeComponent,
    LogoutComponent,
    DslprComponent,
    Co2Component,
    TopAllianceComponent,
    MonitoringComponent,
    ParametreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppBootstrapModule,
    AngularFontAwesomeModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    TabsModule.forRoot(),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: environment.allowedUrls,
        sendAccessToken: true
      }
    }),
    BrowserAnimationsModule,
    FontAwesomeModule,
    NgbModule
  ],
  providers: [OAuthService, GetService],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
