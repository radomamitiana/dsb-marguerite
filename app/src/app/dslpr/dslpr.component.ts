import { Component, OnInit } from '@angular/core';
import * as fileSaver from 'file-saver';
import { DslprService } from '../service/dslpr/dslpr.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { History } from '../data/History';
import { TranslateService } from '@ngx-translate/core';
import { DslprType } from '../utils/DslprType';

@Component({
  selector: 'app-dslpr',
  templateUrl: './dslpr.component.html',
  styleUrls: ['./dslpr.component.css']
})
export class DslprComponent implements OnInit {
  selectedFiles: FileList;
  currentFileUpload: File;
  notification = '';
  isLoading = false;
  isOK = false;
  isKO = false;
  historys: History[];
  progress: { percentage: number } = { percentage: 0 };
  stringQuery: string;

  pageEmpty: true;
  totalPages: 1;
  totalPagesArray = [];
  last: false;
  first: false;
  page = 0;
  type = DslprType.CAUSE_ALE;
  title = 'Mise à jour du fichier "Cause ALE Deviation"';
  classCauseAle = 'btn btn-default bg-warning col-lg-12';
  classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
  classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
  classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
  classCorespondances = 'btn btn-default  bg-dark col-lg-12';
  classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';

  constructor(private dslprService: DslprService, public translate: TranslateService) {
    this.getHistorys(this.page);
  }

  ngOnInit() {
    this.getHistorys(this.page);
  }

  /**
   * Download Cause ALE Deviation file from datalake to local
   */
  onDownload() {
    this.isLoading = true;
    this.dslprService.download(this.type)
      .subscribe(response => {
        let filename = null;
        switch (this.type) {
          case DslprType
            .CAUSE_ALE:
            filename = 'cause_ale_deviation.csv';
            break;
          case DslprType
            .CORRESP_ISO_CODEPAYS:
            filename = 'code_pays.csv';
            break;
          case DslprType
            .CORRESPONDANCES:
            filename = 'table_correspondances.csv';
            break;
          case DslprType
            .FACTOR_CAISSE:
            filename = 'load_factor_caisse.csv';
            break;
          case DslprType
            .FACTOR_COMMERCE:
            filename = 'load_factor_commerce.csv';
            break;
          case DslprType
            .PRIX_MOYEN:
            filename = 'table_prix_moyen.csv';
            break;
          default:
            break;
        }
        this.saveFile(response.body, filename);
        this.isOK = true;
        this.isKO = false;
        this.translate.get('notification.download').subscribe((text: string) => {
          this.notification = text;
        });
        this.getHistorys(0);
        this.isLoading = false;
      });
  }

  /**
   * Save file on local
   * @param data
   * @param filename
   */
  saveFile(data: any, filename?: string) {
    const blob = new Blob([data], { type: 'text/csv; charset=utf-8' });
    fileSaver.saveAs(blob, filename);
  }

  /**
   * Select file to upload
   * @param event
   */
  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  /**
   * uplpoad file from local to the datalake
   */
  onUpload() {
    this.progress.percentage = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.dslprService.upload(this.type, this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
        this.isLoading = true;
      } else if (event instanceof HttpResponse) {

        if (event && event.body) {
          let result = event.body[6] + '' + event.body[7] + '' + event.body[8] + '' + event.body[9];
          if (result === 'true') {
            this.isOK = true;
            this.isKO = false;
            this.translate.get('notification.upload').subscribe((text: string) => {
              this.notification = text;
            });
          } else {
            this.isOK = false;
            this.isKO = true;
            this.translate.get('notification.error').subscribe((text: string) => {
              this.notification = text;
            });

          }
          this.getHistorys(0);
        } else {
          this.isOK = false;
          this.isKO = true;
          this.translate.get('notification.error').subscribe((text: string) => {
            this.notification = text;
          });
        }
        this.isLoading = false;


      }

    });
    this.selectedFiles = undefined;
  }


  /**
   * get all history
   *
   */
  getHistorys(page) {
    console.log('Type => ' + this.type);
    this.dslprService.getHistorys(this.type, page).subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }

  /**
   * check response
   * @param response
   */
  private handleSuccessfulResponse(response) {
    if (response && response.historyDTOS && response.historyDTOS.content) {
      this.historys = response.historyDTOS.content;
      this.pageEmpty = response.historyDTOS.empty;
      this.totalPages = response.historyDTOS.totalPages;
      this.last = response.historyDTOS.last;
      this.first = response.historyDTOS.first;
      this.totalPagesArray = [];
      for (let i = 0; i < this.totalPages; i++) {
        this.totalPagesArray[i] = (i + 1);
      }
    }
  }

  goToPage(page: number) {
    this.page = page - 1;
    if (this.stringQuery && this.stringQuery.trim() !== '') {
      this.searchHistorys();
    } else {
      this.getHistorys(this.page);
    }
  }

  previousPage() {
    this.page = this.page - 1;
    if (this.stringQuery) {
      this.searchHistorys();
    } else {
      this.getHistorys(this.page);
    }
  }

  nextPage() {
    this.page = this.page + 1;
    if (this.stringQuery) {
      this.searchHistorys();
    } else {
      this.getHistorys(this.page);
    }
  }

  setType(type) {
    this.type = type;
    switch (this.type) {
      case DslprType
        .CAUSE_ALE:
        this.classCauseAle = 'btn btn-default bg-warning col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.title = 'Mise à jour du fichier "Cause ALE Deviation"';
        break;
      case DslprType
        .CORRESP_ISO_CODEPAYS:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-warning col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.title = 'Mise à jour du fichier "Code Pays"';
        break;
      case DslprType
        .CORRESPONDANCES:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-warning col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.title = 'Mise à jour du fichier "Table de correspondances"';
        break;
      case DslprType
        .FACTOR_CAISSE:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-warning col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.title = 'Mise à jour du fichier "Load factor caisse"';
        break;
      case DslprType
        .FACTOR_COMMERCE:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-warning col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.title = 'Mise à jour du fichier "Load factor commerce"';
        break;
      case DslprType
        .PRIX_MOYEN:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-warning col-lg-12';
        this.title = 'Mise à jour du fichier "Prix moyen"';
        break;
      default:
        break;
    }
    this.getHistorys(this.page);
  }

  onDownloadErrorReport(errorReport: string) {
    this.dslprService.onDownloadErrorReport(errorReport)
      .subscribe(response => {
        const filename = errorReport;
        this.saveFile(response.body, filename);
        this.isOK = true;
        this.isKO = false;
        this.translate.get('notification.download').subscribe((text: string) => {
          this.notification = text;
        });
        this.getHistorys(0);
      });
  }

  /**
   * search something in the table
   */
  searchHistorys() {
    if (this.stringQuery && this.stringQuery.trim() !== '') {
      console.log('this.type in search => ' + this.type);
      this.dslprService.searchHistorys(this.type, this.stringQuery.trim(), this.page).subscribe(
        response => this.handleSuccessfulResponse(response),
      );
    }
  }
}
