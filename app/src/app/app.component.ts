import { Component } from '@angular/core';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { authConfig } from './auth.config';
import { Router } from '@angular/router';
import { GetService } from './service/get.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  ipn: string;

  userInfoEndpoint = "https://idp2.renault.com/nidp/oauth/nam/userinfo";

  constructor(private oauthService: OAuthService, private router: Router, private _getService: GetService, private translate: TranslateService) {
    translate.setDefaultLang('fr');
    this.configureSingleSignOn();
  }

  configureSingleSignOn() {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndLogin()
      .then(result => {
        if (this.oauthService.hasValidAccessToken()) {
          this.oauthService.getIdentityClaims();
          let accessToken = sessionStorage.getItem('access_token');
          if (accessToken != undefined && accessToken != '' && accessToken != null) {
            this._getService.getUserInfo(this.userInfoEndpoint).subscribe(data => {
              var roleDetails = data['role-dsb-irn70295'];
              console.log(roleDetails);
              if (roleDetails && roleDetails.indexOf("dsb_users") != -1) {
                sessionStorage.setItem("ipn", data.uid);
                this.ipn = data.uid;
                console.log(this.ipn);
                this.router.navigate(['home']);
              } else {
                this.oauthService.logOut();
              }
            });
          }
        } else {
          this.oauthService.initImplicitFlow();
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

}
