import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Constant } from '../../utils/Constant';

export class User {
  constructor(
    public status: string,
  ) {
  }

}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  /**
   * chech if a session exist for this user
   */
  public isUserLoggedIn() {
    const user = sessionStorage.getItem('ipn');
    return !(user === null);
  }
}
