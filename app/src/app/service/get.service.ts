import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const API_URL = window.location.protocol + '//' + window.location.host + "/api/";

@Injectable()
export class GetService {

    constructor(private _http: HttpClient) { }

    public getRequest(getUrl: string): Observable<any> {
        return this._http.get(API_URL + getUrl);
    }

    public getUserInfo(getUrl: string): Observable<any> {
        let accessToken = sessionStorage.getItem('access_token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + accessToken,
            })
        };
        return this._http.get(getUrl, httpOptions);
    }
}
