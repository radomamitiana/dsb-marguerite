import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Account } from '../../data/Account';
import { Constant } from '../../utils/Constant';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * get users's information
   */
  getAccount(ipn) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.get<Account>(Constant.URL_API_ROOT + '/api/user/' + sessionStorage.getItem('ipn') + '/roles', {headers});
  }
}
