import {HttpClient, HttpEvent, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {History} from '../../data/History';
import {Constant} from '../../utils/Constant';

const API_URL = Constant.URL_API_ROOT;

@Injectable({
  providedIn: 'root'
})
export class DslprService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Download Cause ALE Deviation file from datalake to local
   */

  download(type: string): Observable<HttpResponse<String>> {
    return this.httpClient.get(API_URL + '/api/dslpr/download?type=' + type +'&ipn=' +sessionStorage.getItem('ipn'), {
      observe: 'response',
      responseType: 'text'
    });
  }


  /**
   * Download Error report
   */

  onDownloadErrorReport(errorReport: string): Observable<HttpResponse<String>> {
    return this.httpClient.get(API_URL + '/api/dslpr/downloadErrorReport/' + errorReport, {
      observe: 'response',
      responseType: 'text'
    });
  }

  /**
   * upload file from local to the datalake
   * @param type
   * @param file
   */
  upload(type: string, file: File): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', API_URL + '/api/dslpr/upload?type=' + type +'&ipn=' +sessionStorage.getItem('ipn'), formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.httpClient.request(req);
  }

  /**
   * get all history
   * @param type
   * @param page
   */
  getHistorys(type: string, page) {
    return this.httpClient.get<History[]>(API_URL + '/api/history/dslpr/list?page=' + page + '&type=' + type);
  }

  /**
   * search  history
   * @param type
   * @param stringQuery
   * @param page
   */
  searchHistorys(type: string, stringQuery, page) {
    return this.httpClient.get<History[]>(API_URL + '/api/history/dslpr/list?stringQuery=' + stringQuery + '&page=' + page + '&type=' + type);
  }

}
