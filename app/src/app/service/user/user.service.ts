import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../data/User';
import { Constant } from '../../utils/Constant';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * get all user
   */
  getUsers(page) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.get<User[]>(Constant.URL_API_ROOT + '/api/user/list?page=' + page, { headers });
  }

  /**
   * add an user
   * @param ipn
   * @param strRoles
   */
  addUser(ipn, strRoles) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.post<User>(Constant.URL_API_ROOT + '/api/user/add', { ipn, strRoles }, { headers });
  }

  /**
   * update user's role
   * @param id
   * @param dropdownDataDTOS
   */
  updateUser(id, dropdownDataDTOS) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.put<User>(Constant.URL_API_ROOT + '/api/user/update', { id, dropdownDataDTOS }, { headers });
  }

  /**
   * delete user
   * @param id
   */
  deleteUser(id) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.delete<User>(Constant.URL_API_ROOT + '/api/user/delete?id=' + id, { headers });
  }

  /**
   * search  user
   * @param stringQuery
   * @param page
   */
  searchUsers(stringQuery, page) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.get<User[]>(Constant.URL_API_ROOT + '/api/user/list?stringQuery=' + stringQuery + '&page=' + page, { headers });
  }
}
