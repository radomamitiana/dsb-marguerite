import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Setting } from '../../data/Setting';
import { Constant } from '../../utils/Constant';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * get all setting
   */
  getSettings(page) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.get<Setting[]>(Constant.URL_API_ROOT + '/api/setting/list?page=' + page, { headers });
  }

  /**
   * search  setting
   * @param stringQuery
   * @param page
   */
  searchSettings(stringQuery, page) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.get<Setting[]>(Constant.URL_API_ROOT + '/api/setting/list?stringQuery=' + stringQuery + '&page=' + page, { headers });
  }

  /**
   * create or update an setting
   * @param name
   * @param value
   * @param isCrypt
   */
  createOrUpdateSetting(id, name, value, isCrypt) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.post<Setting>(Constant.URL_API_ROOT + '/api/setting/createOrUpdate', {
      id,
      name,
      value,
      isCrypt
    }, { headers });
  }

  /**
   * delete setting
   * @param id
   */
  deleteSetting(id) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.delete<Setting>(Constant.URL_API_ROOT + '/api/setting/delete?id=' + id, { headers });
  }

}
